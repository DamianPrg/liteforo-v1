# liteforo-v1

[![Join the chat at https://gitter.im/DamianPrg/liteforo-v1](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/DamianPrg/liteforo-v1?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

forum powered by laravel

little project in laravel 5

### Development setup
1. place project in your webserver directory
2. run 'composer install'
3. copy .env.example as .env and fill your database connection details
4. navigate to http://your_web_server/install to migrate and seed database
